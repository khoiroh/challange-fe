import React from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import Home from "./pages/Home";
import Login from "./pages/Login";
import Register from "./pages/Register";

const App = () => {
  return (
    <div className="">
      <div className="app-wrapper">
        <BrowserRouter>
          <main>
            <Switch>
                <Route path="/login" component={Login} exact />
                <Route path="/register" component={Register} exact />
              <div>
                <Route path="/home" component={Home} exact />
              </div>
            </Switch>
          </main>
        </BrowserRouter>
      </div>
    </div>
  );
};

export default App;
