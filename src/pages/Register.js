import axios from "axios";
import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import Swal from "sweetalert2";

export default function () {
  const [username, setUsername] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const register = async (e) => {
    e.preventDefault();

    try {
      await axios.post("http://localhost:3010/login1/sign-up", {
        email: email,
        password: password,
        username: username,
      });
      Swal.fire({
        icon: "success",
        title: "Register berhasil!!!",
        showCancelButton: false,
        timer: 1500,
      });
      setTimeout(() => {
        window.location.reload();
        history.push("/login");
      }, 1500);
    } catch (error) {
      console.log(error);
    }
  };
  const history = useHistory();

  return (
    <div className="block md:flex">
      <div className="mt-8 p-5">
        <div className="text-green-700 font-semibold">
          <h1 style={{ textAlign: "center", fontSize: 40 }}>Formulir Daftar</h1>
        </div>
        <img
          className="w-1/2 ml-20 md:w-2/3 mt-5"
          src="https://cdn-icons-png.flaticon.com/512/166/166260.png"
          alt=""
        />
      </div>
      <div
        style={{ padding: 15, borderRadius: 10 }}
        className="container mt-10 w-full md:w-1/3"
      >
        <form
          onSubmit={register}
          method="POST"
          class="backdrop-opacity-20 backdrop-invert bg-white/0 shadow-md rounded px-8 pt-6 pb-8 mb-4"
        >
          <div class="mb-4 mt-2">
            <label class="block text-gray-700 text-sm font-bold mb-2">
              Username
            </label>
            <input
              class="backdrop-opacity-25 backdrop-invert bg-white/0 shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
              type="text"
              placeholder="Username"
              value={username}
              onChange={(e) => setUsername(e.target.value)}
            />
          </div>
          <div class="mb-4 mt-2">
            <label class="block text-gray-700 text-sm font-bold mb-2">
              Email
            </label>
            <input
              class="backdrop-opacity-25 backdrop-invert bg-white/0 shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
              type="text"
              placeholder="Email"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
            />
          </div>
          <div class="mb-3">
            <label class="block text-gray-700 text-sm font-bold mb-2">
              Password
            </label>
            <input
              class="backdrop-opacity-25 backdrop-invert bg-white/0 shadow appearance-none border border-red-500 rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline"
              type="password"
              placeholder="******************"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
            />
            {/* <p class="text-red-700 text-xs italic">Please choose a password.</p> */}
          </div>
          <div class="flex items-center justify-between">
            <button
              class="bg-green-600 hover:bg-green-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
              type="submit"
            >
              Mendaftar
            </button>
            <a
              class="inline-block align-baseline font-bold text-sm text-green-600 hover:text-green-800"
              href="/login"
            >
              Sudah memiliki akun? Gabung
            </a>
          </div>
        </form>
      </div>
    </div>
  );
}
